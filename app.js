
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();
const emailfun= require("./node-mailer")


mongoose.connect(process.env.MONGO_CONNECTION_URL).then(()=>console.log("Connected to the cloud Successfully")).catch((error)=>console.log(error));
emailfun;

const port = process.env.PORT;
app.listen(port,()=>console.log("server started at "+port))

